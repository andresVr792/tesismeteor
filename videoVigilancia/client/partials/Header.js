Template.Header.events({
  "click .login-toggle": function(){
     Session.set('nav-toggle', 'open');
  },
  "click .logout": ()=> {
    AccountsTemplates.logout();
  }
});
