
Template.Users.onCreated(function () {



});
Template.ModoSeguro.helpers({
    modoSeguroOption:()=>{
        return Session.get('modoSeguridad')==true;
    }
});

Template.ModoSeguro.events({
    'change .toggleModoSeguro':function (events,template) {

        var flag=false
        var x=$('#modoSeguro').prop('checked');

        if(x){
            Meteor.call("callSecurityModeOn");
            Session.setPersistent('modoSeguridad',true);
        }
        else{
            Meteor.call("callSecurityModeOff");
            Session.setPersistent('modoSeguridad',false);
        }
    }
});