Template.EditUser.onCreated(function() {
  this.editMode = new ReactiveVar(false);
  this.tmp_roles = new ReactiveVar([]);

  this.autorun(() => {
    this.subscribe('allRoles');
  });

  if (this.data.roles) {
    this.tmp_roles = new ReactiveVar(this.data.roles);
  }


});


Template.EditUser.helpers({
  updateRecipeId: function () {
    return this._id;
  },
  editMode: function () {
    return Template.instance().editMode.get();
  },
  tmp_roles: function () {
    return Template.instance().tmp_roles.get();
  },
  all_roles: function () {
    return Roles.getAllRoles();
  }
})

Template.EditUser.events({
  'submit form': function(event, template){
    event.preventDefault();
    console.log(event);
    var email = event.target.emailInput.value;
    var name= event.target.nameInput.value;
    var cedula= event.target.cedulaInput.value;
    var roles_list=template.tmp_roles.get();

    var id = Template.instance().data._id;
      console.log('id:' + id+ ' name:'+name+ ' cedula: '+cedula);
      Meteor.users.update(id, {
        $set: {'emails.0.address': email,
            'profile.name': name,
            'profile.cedula': cedula,
            'roles': roles_list}
      })

    console.log(email);
    //Roles.createRole(playerNameVar);
  },
  'click .fa-pencil': function(event, template) {
      
      template.editMode.set(!template.editMode.get());
      console.log('click fa-pencil: '+template.editMode.get());
      //Session.set('editMode', !Session.get('editMode'));
  },
  'click .fa-trash': function() {
    console.log('delete');
    Meteor.call('deleteRule', this._id);
  },
  'click .remove-role': function(event, template) {
    console.log(this);
    if (true) {
      var list = Template.instance().tmp_roles.get();
      console.log(list);
      remove(list, this.valueOf());
      Template.instance().tmp_roles.set(list);
    } else {
      console.log('You need to be an admin!');
    }

  },
  'click .add-role': function(event, template) {
    event.preventDefault();
    var list=Template.instance().tmp_roles.get();
    var a = template.find('#select_input');
    a = $(a).val();
    list.push(a);
    Template.instance().tmp_roles.set(list);
  }
});

function remove(arr, item) {
  for (var i = arr.length; i--;) {
    if (arr[i] === item) {
      arr.splice(i, 1);
    }
  }
}