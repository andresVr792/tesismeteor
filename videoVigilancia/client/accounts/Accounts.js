var myLogoutFunc=function () {
    Session.set('nav-toggle','');
    FlowRouter.go('/');
    Session.setPersistent('currentUser',null);
    Session.setPersistent('logedUser',null);
    Session.keys={};
}
var myLoginFunc=function () {
    Session.setPersistent('logedUser', Meteor.user());
    if(Session.get('logedUser')!=null) {
        FlowRouter.go('/dashboard');

    }
}

AccountsTemplates.configure({
    confirmPassword: false,

    onLogoutHook: myLogoutFunc,
    onSubmitHook:myLoginFunc
});

AccountsTemplates.addFields([

    {
        _id: 'firstName',
        type: 'text',
        displayName: 'Nombre1',
        placeholder:"Primer Nombre",
        required:true,
        upperCase:true,

    },
    {
        _id: 'midleName',
        type: 'text',
        displayName: 'Nombre2',
        placeholder:"Segundo Nombre",
        required:true,
        upperCase:true,

    },
    {
        _id: 'lastName',
        type: 'text',
        displayName: 'Apellido',
        placeholder:'Primer Apellido',
        upperCase:true,
        required:true,


    },
    {
        _id: 'lastName2',
        type: 'text',
        displayName: 'Apellido2',
        placeholder:'Segundo Apellido',
        upperCase:true,
        required:true,


    },
    {
        _id: 'areaConocimiento',
        type: 'select',
        displayName: 'A. Conocimiento ',
        required:true,
        select:[
            {
                text: "seleccionar...",
                value: "",
            },
            {
                text: "Ciencias naturales",
                value: "CIENCIAS NATURALES",
            },
            {
                text: "Ciencias sociales",
                value: "CIENCIAS SOCIALES",
            },

            {
                text: "Educación cultual y artística",
                value: "EDUCACION CULTURAL Y ARTISTICA",
            },
            {
                text: "Educación física",
                value: "EDUCACION FISICA",
            }
            ,
            {
                text: "EGB-E",
                value: "EGBE",
            },
            {
                text: "Formación Cristiana ",
                value: "FORMACIÓN CRISTIANA",
            },
            {
                text: "Inglés ",
                value: "INGLES",
            },
            {
                text: "Lengua y literatura ",
                value: "LENGUALITERA",
            },
            {
                text: "Matemática",
                value: "MATEMATICA",
            }

        ],


    },
    {
        _id: 'subnivel',
        type: 'select',
        displayName: 'Subnivel Educativo',
        required:true,
        select:[
            {
                text: "seleccionar...",
                value: "",
            },
            {
                text: "Preparatorio",
                value: "PREPARATORIO",
            },
            {
                text: "EGB-E",
                value: "EGBE",
            },

            {
                text: "EGB-M",
                value: "EGBM",
            },
            {
                text: "EGB-S",
                value: "EGBS",
            }
            ,
            {
                text: "BGU",
                value: "BGU",
            }
        ],


    }

]);

