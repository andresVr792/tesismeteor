var postSignUp=function (userId,info) {
    Roles.addUsersToRoles(userId, ['user']);
}

AccountsTemplates.configure({
    postSignUpHook: postSignUp
});

