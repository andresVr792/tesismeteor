Meteor.methods({
    toggleAdmin(id){
        if (Roles.userIsInRole(id, 'admin')) {
            Roles.removeUsersFromRoles(id, 'admin');
        }
        else {
            Roles.addUsersToRoles(id, 'admin');
        }
    },
    toggleUser(id){
        if (Roles.userIsInRole(id, 'user')) {
            Roles.removeUsersFromRoles(id, 'user');
        }
        else {
            Roles.addUsersToRoles(id, 'user');
        }
    },
    sessionUser(){
        return Meteor.users.find({});
    },
    reviewRepetitions(collection,value){
        return _.some(collection, function (valor) {
            return valor.texto == value;
        });
    },
    'iniciarBase':function () {

        this.unblock();
        // Construct the API URL
        var apiUrl = 'http://190.15.140.7:4848/todo/api/v1.0/iniciarBase';
        // query the API
        var response = HTTP.post(apiUrl);
        return 1;

    },
    'finalizarSesion':function () {

        this.unblock();
        // Construct the API URL
        var apiUrl = 'http://190.15.140.7:4848/todo/api/v1.0/finalizarSesion';
        // query the API
        var response = HTTP.post(apiUrl);
        return 1;

    },
    'callSecurityModeOn':function () {

        this.unblock();
        // Construct the API URL
        var apiUrl = 'http://192.168.100.7:5678/todo/api/v1.0/tasks';
        // query the API
        var response = HTTP.post(apiUrl);
        return 1;

    },
    'callSecurityModeOff':function () {

        this.unblock();
        // Construct the API URL
        var apiUrl = 'http://192.168.100.7:4848/todo/api/v1.0/tasks';
        // query the API
        var response = HTTP.post(apiUrl);
        return 1;

    },
    'meses':function () {

            return Alertas.find({$and:[{fecha:{$gte:new Date("2016-11-01")},fecha:{$lte:new Date("2016-11-30")}}]}).count();
        ;

    }






});