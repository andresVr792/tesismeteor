Meteor.publish('allUsers',function () {
    if(Roles.userIsInRole(this.userId,'admin')) {
        return Meteor.users.find({});
    }
});

Meteor.publish('filterUser',function (rol) {

        return Meteor.users.find({roles:{$in:[rol]}});

});
Meteor.publish('users',function () {

        return Meteor.users.find({});

});

Meteor.publish('alertas',function () {

    return Alertas.find();

});
Meteor.publish('luzDelantero',function () {

   return LuzDelantera.find({$and:[{fecha:{$gte:new Date("2016-10-20")},fecha:{$lte:new Date()}}]},{limit:100});

});
