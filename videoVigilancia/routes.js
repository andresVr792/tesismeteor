if(Meteor.isClient) {
    Accounts.onLogin(function () {
            FlowRouter.go('dashboard');
        }
    );
    Accounts.onLogout(function () {
            FlowRouter.go('home');
        }
    );
}

// Home Page
FlowRouter.route('/', {
    name: 'home',
    action() {
        BlazeLayout.render("HomeLayout", {main: "Home"});
    }
});

// Home Dashboard
FlowRouter.route('/dashboard', {
    name: 'dashboard',
    action() {
        BlazeLayout.render("AppLayout", {main: "Dashboard"});
    }
});

// user account

var adminRoutes=FlowRouter.group({
    prefix: '/admin',
    name: 'admin'
});

adminRoutes.route('/users', {
    name:'users',
    action() {
        BlazeLayout.render("AppLayout", {main: "Users"});
    }
});
//planES
var videoStreamingRoutes=FlowRouter.group({
    prefix: '/streaming',
    name: 'streaming'
});
videoStreamingRoutes.route('/modoSeguro', {
    name:'modoSeguro',
    action() {
        BlazeLayout.render("AppLayout", {main: "ModoSeguro"});
    }
});
